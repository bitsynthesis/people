# Guaranteed Rate People API

## Build

    lein uberjar

## Arguments

leiningen

    lein run -- --help

uberjar

    java -jar target/guaranteed-rate-2.0.0-standalone.jar --help

## Examples

leiningen

    lein run -- \\
      --file ./test-resources/comma.txt \\
      --file ./test-resources/pipe.txt \\
      --sort last-name \\
      --app

uberjar

    java -jar target/guaranteed-rate-1.0.0-standalone.jar \\
      --file ./test-resources/comma.txt \\
      --file ./test-resources/pipe.txt \\
      --sort last-name \\
      --app

If --app specificed, HTTP server can be reached at http://localhost:3000
