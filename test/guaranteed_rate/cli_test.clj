(ns guaranteed-rate.cli-test
  (:require [clojure.java.io :as io]
            [clojure.pprint :as pp]
            [clojure.test :refer :all]
            [guaranteed-rate.cli :as cli]
            [guaranteed-rate.people :as people]
            [guaranteed-rate.test-helpers :refer :all]))


(use-fixtures :each people-fixture)


(def test-file-cli-args
  ["-f" (io/resource "pipe.txt")
   "-f" (io/resource "comma.txt")
   "-f" (io/resource "space.txt")])


(defn get-printed-output [cli-args]
  (let [printed (atom nil)]
    (with-redefs [pp/print-table (partial reset! printed)]
      (apply cli/-main cli-args)
      @printed)))


(deftest parses-files
  (is (= (set (map #(dissoc % :date-of-birth)
                   expected-unsorted))
         (set (map #(dissoc % :date-of-birth)
                   (get-printed-output test-file-cli-args))))))


(deftest sort-by-gender
  (is (= (map #(dissoc % :date-of-birth)
              expected-sorted-by-gender)
         (map #(dissoc % :date-of-birth)
              (get-printed-output
               (concat ["-s" "gender"] test-file-cli-args))))))


(deftest sort-by-date-of-birth
  (is (= (map #(dissoc % :date-of-birth)
              expected-sorted-by-date-of-birth)
         (map #(dissoc % :date-of-birth)
              (get-printed-output
               (concat ["-s" "date-of-birth"] test-file-cli-args))))))


(deftest sort-by-last-name
  (is (= (map #(dissoc % :date-of-birth)
              expected-sorted-by-last-name)
         (map #(dissoc % :date-of-birth)
              (get-printed-output
               (concat ["-s" "last-name"] test-file-cli-args))))))


(deftest format-printed-date
  (is (= #{"7/12/1991"
           "6/10/1994"
           "4/12/1996"
           "9/3/1997"
           "3/31/1999"
           "5/7/1999"}
         (set (map :date-of-birth
                   (get-printed-output test-file-cli-args))))))


(deftest stores-parsed-records
  (get-printed-output test-file-cli-args)
  (is (= (set expected-unsorted)
         (set (people/get-people "none")))))


(deftest throw-exception-on-missing-file
  (is (thrown? java.lang.IllegalArgumentException
               (cli/-main "-f" "notreal.txt"))))


(deftest throw-exception-on-unsupported-sort
  (is (thrown? java.lang.IllegalArgumentException
               (apply cli/-main "-s" "wrong" test-file-cli-args))))
