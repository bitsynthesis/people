(ns guaranteed-rate.people-test
  (:require [clojure.test :refer :all]
            [guaranteed-rate.people :as people]
            [guaranteed-rate.test-helpers :refer :all]))


(use-fixtures :each people-fixture)


(deftest parses-pipe-delimited-record
  (is (= [johnny-utah]
         (#'people/create-people
          "Utah | Johnny | Male | Green | 1991-7-12"))))


(deftest parses-comma-delimited-record
  (is (= [johnny-utah]
         (#'people/create-people "Utah, Johnny, Male, Green, 1991-7-12"))))


(deftest parses-space-delimited-record
  (is (= [johnny-utah]
         (#'people/create-people "Utah Johnny Male Green 1991-7-12"))))


(deftest parses-multiple-records
  (is (= [johnny-utah vanessa-lutz]
         (#'people/create-people
          (str "Utah | Johnny | Male | Green | 1991-7-12\n"
               "Lutz | Vanessa | Female | Red | 1997-9-3")))))


(deftest throws-error-on-invalid-record
  (is (thrown-with-msg?
       java.lang.IllegalArgumentException
       #"^Person record has invalid number of fields: John \| Doe$"
       (doall (#'people/create-people
               "John | Doe\nLutz | Vanessa | Female | Red | 1997-9-3")))))
