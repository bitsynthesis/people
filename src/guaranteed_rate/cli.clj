(ns guaranteed-rate.cli
  (:require [clj-time.format :as timef]
            [clojure.java.io :as io]
            [clojure.pprint :as pp]
            [clojure.string :as string]
            [clojure.tools.cli :as tools-cli]
            [guaranteed-rate.http :as http]
            [guaranteed-rate.people :as people]
            [mount.core :as mount])
  (:gen-class))


(def ^:private standard-date-formatter
  (timef/formatter "YYYY-MM-dd"))


(def ^:private printed-date-formatter
  (timef/formatter "M/d/YYYY"))


(defn ^:private parse-standard-date [k record]
  (update-in record [k] (partial timef/parse standard-date-formatter)))


(defn ^:private format-printed-date [k record]
  (update-in record [k] (partial timef/unparse printed-date-formatter)))


(defn ^:private format-dates-for-printing [records]
  (map (comp (partial format-printed-date :date-of-birth)
             (partial parse-standard-date :date-of-birth))
       records))


(defn ^:private load-files [files]
  (mapcat (comp people/create-people slurp) files))


(def ^:private cli-interface
  [["-a" "--app" "Run http server on port 3000"]
   ["-f"
    "--file PATH"
    "Data file to parse, supports multiple -f definitions"
    :id :files
    :default []
    :validate [(comp #(.exists %) io/file) "File must exist"]
    :assoc-fn (fn [m k v] (update-in m [k] conj v))]
   ["-s"
    "--sort COLUMN"
    (format "Column to sort by (%s)"
            (string/join ", " (keys people/sort-fns)))
    :default "none"
    :validate [(comp not nil? people/sort-fns)
               "Column not supported for sorting"]]
   ["-h" "--help"]])


(defn -main [& args]
  (let [parsed-args (tools-cli/parse-opts args cli-interface)]
    (when-let [errors (parsed-args :errors)]
      (throw (java.lang.IllegalArgumentException. (string/join "\n" errors))))

    (when (-> parsed-args :options :help)
      (println (parsed-args :summary)))

    (load-files (-> parsed-args :options :files))

    (->> (people/get-people (-> parsed-args :options :sort))
         format-dates-for-printing
         pp/print-table)

    (when (-> parsed-args :options :app)
      (mount/start #'guaranteed-rate.http/http-server))))
