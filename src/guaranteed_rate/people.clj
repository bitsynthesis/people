(ns guaranteed-rate.people
  (:require [clojure.string :as string]))


(def people-table (atom []))


(defrecord ^:private Person
  [last-name first-name gender favorite-color date-of-birth])


(defn ^:private parse-person [record-string]
  (let [values (->> (string/split record-string #"\||,")
                    (map string/trim)
                    (mapcat #(string/split % #" ")))]
    (try
     (apply ->Person values)
     (catch clojure.lang.ArityException e
       (throw
        (java.lang.IllegalArgumentException.
         (format "Person record has invalid number of fields: %s"
                 record-string)))))))


(defn ^:private sort-by-gender [people]
  (sort #(if (not= (:gender %1) (:gender %2))
           (compare (:gender %1) (:gender %2))
           (compare (:last-name %1) (:last-name %2)))
        people))


(defn ^:private sort-by-date-of-birth [people]
  (sort-by :date-of-birth people))


(defn ^:private sort-by-last-name [people]
  (reverse (sort-by :last-name people)))


(def sort-fns
  {"date-of-birth" sort-by-date-of-birth
   "gender" sort-by-gender
   "last-name" sort-by-last-name
   "none" identity})


(defn create-people [records-string]
  (let [parsed (->> records-string
                    string/split-lines
                    (map parse-person)
                    (remove nil?))]
    (swap! people-table concat parsed)
    parsed))


(defn get-people [sort-column]
  (let [sort-fn (get sort-fns sort-column)]
    (sort-fn @people-table)))


(defn delete-people []
  (reset! people-table []))
