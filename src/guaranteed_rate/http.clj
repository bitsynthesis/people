(ns guaranteed-rate.http
  (:require [clojure.data.json :as json]
            [compojure.core :as serve]
            [compojure.route :as route]
            [guaranteed-rate.people :as people]
            [mount.core :as mount]
            [ring.adapter.jetty :as jetty]))


(defn ^:private json-response
  ([body] (json-response body 200))
  ([body status]
   {:status status
    :headers {"content-type" "application/json"}
    :body (json/write-str (or body {}))}))


(serve/defroutes ^:private routes
  (serve/POST "/records" {body :body}
    (try
     (json-response (people/create-people (slurp body)) 201)
     (catch java.lang.IllegalArgumentException e
       (json-response (.getMessage e) 400))))
  (serve/GET "/records/birthdate" []
    (json-response (people/get-people "date-of-birth")))
  (serve/GET "/records/gender" []
    (json-response (people/get-people "gender")))
  (serve/GET "/records/name" []
    (json-response (people/get-people "last-name"))))


(mount/defstate http-server
  :start (jetty/run-jetty routes {:port 3000, :join? true})
  :stop (.stop http-server))
