(ns guaranteed-rate.http-test
  (:require [clojure.data.json :as json]
            [clojure.test :refer :all]
            [guaranteed-rate.http :as http]
            [guaranteed-rate.people :as people]
            [guaranteed-rate.test-helpers :refer :all]
            [ring.mock.request :as mock-request]))


(defn test-data-fixture [t]
  (reset! (deref #'people/people-table) expected-unsorted)
  (t))


(use-fixtures :each (compose-fixtures people-fixture test-data-fixture))


(deftest get-sorted-by-gender
  (is (= {:status 200
          :headers {"content-type" "application/json"}
          :body (json/write-str expected-sorted-by-gender)}
       (#'http/routes (mock-request/request :get "/records/gender")))))


(deftest get-sorted-by-date-of-birth
  (is (= {:status 200
          :headers {"content-type" "application/json"}
          :body (json/write-str expected-sorted-by-date-of-birth)}
       (#'http/routes (mock-request/request :get "/records/birthdate")))))


(deftest get-sorted-by-last-name
  (is (= {:status 200
          :headers {"content-type" "application/json"}
          :body (json/write-str expected-sorted-by-last-name)}
       (#'http/routes (mock-request/request :get "/records/name")))))


(deftest create-person
  (people/delete-people)
  (let [test-record "Utah | Johnny | Male | Green | 1991-7-12"]
    (is (= {:status 201
            :headers {"content-type" "application/json"}
            :body (json/write-str [johnny-utah])}
           (#'http/routes
            (mock-request/request :post "/records" test-record))))
    (is (= {:status 200
            :headers {"content-type" "application/json"}
            :body (json/write-str [johnny-utah])}
           (#'http/routes (mock-request/request :get "/records/gender"))))))


(deftest bad-create-person-request-returns-error
  (is (= {:status 400
          :headers {"content-type" "application/json"}
          :body "\"Person record has invalid number of fields: Bad Data\""}
         (#'http/routes (mock-request/request :post "/records" "Bad Data")))))
