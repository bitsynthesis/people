(defproject guaranteed-rate "2.0.0"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-time "0.13.0"]
                 [mount "0.1.11"]
                 [ring "1.6.1"]
                 [compojure "0.6.0"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/tools.cli "0.3.5"]]
  :main guaranteed-rate.cli
  :profiles {:dev {:dependencies [[ring/ring-mock "0.3.0"]]
                   :plugins [[lein-auto "0.1.3"]
                             [lein-cloverage "1.0.9"]]}
             :uberjar {:aot :all}}
  :test-paths ["test" "test-resources"])
