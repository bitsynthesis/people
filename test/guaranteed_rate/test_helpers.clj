(ns guaranteed-rate.test-helpers
  (:require [guaranteed-rate.people :as people]))


(def johnny-utah
  (people/->Person "Utah" "Johnny" "Male" "Green" "1991-7-12"))


(def vanessa-lutz
  (people/->Person "Lutz" "Vanessa" "Female" "Red" "1997-9-3"))


(def tracy-flick
  (people/->Person "Flick" "Tracy" "Female" "Yellow" "1999-5-7"))


(def thomas-anderson
  (people/->Person "Anderson" "Thomas" "Male" "Blue" "1999-3-31"))


(def jack-traven
  (people/->Person "Traven" "Jack" "Male" "Red" "1994-6-10"))


(def nicole-walker
  (people/->Person "Walker" "Nicole" "Female" "White" "1996-4-12"))


(def expected-unsorted
  [johnny-utah
   vanessa-lutz
   tracy-flick
   thomas-anderson
   jack-traven
   nicole-walker])


(def expected-sorted-by-gender
  [tracy-flick
   vanessa-lutz
   nicole-walker
   thomas-anderson
   jack-traven
   johnny-utah])


(def expected-sorted-by-date-of-birth
  [johnny-utah
   jack-traven
   nicole-walker
   vanessa-lutz
   thomas-anderson
   tracy-flick])


(def expected-sorted-by-last-name
  [nicole-walker
   johnny-utah
   jack-traven
   vanessa-lutz
   tracy-flick
   thomas-anderson])


(defn people-fixture [t]
  (people/delete-people)
  (t)
  (people/delete-people))
